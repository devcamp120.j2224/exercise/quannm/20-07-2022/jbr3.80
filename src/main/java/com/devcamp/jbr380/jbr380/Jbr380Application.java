package com.devcamp.jbr380.jbr380;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Jbr380Application {

	public static void main(String[] args) {
		SpringApplication.run(Jbr380Application.class, args);
	}

}
