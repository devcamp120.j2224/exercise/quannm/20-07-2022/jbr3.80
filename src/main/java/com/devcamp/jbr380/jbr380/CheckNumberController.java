package com.devcamp.jbr380.jbr380;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CheckNumberController {
    @CrossOrigin
    @GetMapping("/checknumber")
    public String getCheckNumber() {
        int inp = 12344;
        String out;
        if (inp % 2 != 0) {
            out = "input là số lẻ";
        }
        else {
            out = "input là số chẵn";
        }
        return out;
    }
}
